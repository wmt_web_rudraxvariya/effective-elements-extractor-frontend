import React from "react";
import Logo from "./Artboard.png";
export const Header = () => {
  return (
    <div style={{ padding: "10px", backgroundColor:'black' }}>
      <img
        src={Logo}
        alt="X-Path Logo"
        style={{ width: "100px" }}
      />
    </div>
  );
};
