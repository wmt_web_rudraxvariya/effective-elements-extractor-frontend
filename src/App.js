import "./App.css";
import Footer from "./Footer";
import SearchForm from "./SearchForm";
import 'react-notifications/lib/notifications.css';

function App() {
  return (
    <div className="App">
      <SearchForm />
      {/* <Footer /> */}
    </div>
  );
}

export default App;
