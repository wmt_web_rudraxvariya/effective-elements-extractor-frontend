import React, { useState } from "react";
import backgroundImg from "./black-background.jpg";
import axios from "axios";
import Logo from "./Artboard.png";
import Loader from "./Loader";
import { Prism as SyntaxHighlighter } from "react-syntax-highlighter";
import { darcula } from "react-syntax-highlighter/dist/esm/styles/prism";
import {
  NotificationContainer,
  NotificationManager,
} from "react-notifications";

function SearchForm() {
  const [data, setData] = useState("");
  const [loading, setLoading] = useState(false);
  const [copy, setCopy] = useState(false);
  const handleSubmit = async (e) => {
    e.preventDefault();
    const url = e.target[0].value;
    try {
      setLoading(true);
      const res = await axios.post("http://localhost:8000/getXpath", {
        url,
      });
      if (res.data.length <= 0) alert("No data found");
      setData(res.data);
      setLoading(false);
    } catch (error) {
      setLoading(false);
      console.log("error", error);
      alert(error.message);
    }
  };
  console.log("asd", data.length);

  return (
    <>
      {loading ? (
        <Loader />
      ) : (
        <form onSubmit={handleSubmit}>
          <div
            style={{
              backgroundImage: `url(${backgroundImg})`,
              minHeight: "100vh",
              display: "flex",
              backgroundSize: "cover",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <div
              style={{
                width: data.length > 0 ? "80%" : "400px",
                padding: "40px",
                backgroundColor: "black",
                border: "4px solid white",
                margin: "30px 0px",
              }}
            >
              {data.length === 0 ? (
                <>
                  <img
                    src={Logo}
                    alt="X-Path Logo"
                    style={{
                      width: "170px",
                      marginBottom: "33px",
                      marginTop: "-8px",
                      marginLeft: "100px",
                    }}
                  />
                  <br />
                  <label style={{ color: "white" }}>Enter URL:</label>
                  <input
                    type="text"
                    required
                    placeholder="Enter URL"
                    style={{
                      display: "block",
                      margin: "20px 0",
                      padding: "10px",
                      borderRadius: "5px",
                      width: "377px",
                      border: "1px solid #29ABE2",
                    }}
                  />
                  {/* <label style={{ color: "white" }}>Keywords:</label>
                  <input
                    type="text"
                    placeholder="Enter Keywords"
                    required
                    style={{
                      display: "block",
                      margin: "20px 0",
                      padding: "10px",
                      width: "377px",
                      borderRadius: "5px",
                      border: "1px solid #29ABE2",
                    }}
                  /> */}
                  <button
                    style={{
                      backgroundColor: "#29ABE2",
                      color: "black",
                      margin: "15px 0px",
                      padding: "10px 30px",
                      borderRadius: "5px",
                      border: "none",
                      cursor: "pointer",
                    }}
                  >
                    Search
                  </button>
                </>
              ) : (
                <>
                  <button
                    onClick={() => {
                      navigator.clipboard
                        .writeText(data)
                        .then(() => {
                          setCopy(true);
                          setTimeout(() => {
                            setCopy(false);
                          }, 2000);
                        })
                        .catch((err) => {
                          console.log(err);
                        });
                    }}
                    style={{
                      backgroundColor: "#29ABE2",
                      color: "black",
                      margin: "15px 0px",
                      padding: "10px 20px",
                      borderRadius: "5px",
                      border: "none",
                      cursor: "pointer",
                    }}
                  >
                    Copy text
                  </button>
                  {copy && <p style={{ color: "cyan" }}>successfully copied</p>}
                  <SyntaxHighlighter language={"javascript"} style={darcula}>
                    {data}
                  </SyntaxHighlighter>
                  <button
                    onClick={() => setData("")}
                    style={{
                      backgroundColor: "#29ABE2",
                      color: "black",
                      margin: "15px 0px",
                      padding: "10px 20px",
                      borderRadius: "5px",
                      border: "none",
                      cursor: "pointer",
                    }}
                  >
                    Search Again
                  </button>
                </>
              )}
            </div>
          </div>
        </form>
      )}
    </>
  );
}

export default SearchForm;
