import Lottie from "react-lottie";
import animationData from "./loading.json";
import React from "react";

const defaultOptions = {
  loop: true,
  autoplay: true,
  animationData: animationData,
  rendererSettings: {
    preserveAspectRatio: "xMidYMid slice",
  },
};
const Loader = () => {
  return (
    <>
      <Lottie options={defaultOptions} height={400} width={400} />
      <h2 style={{textAlign:'center'}}>Please wait while we load your X-Path!</h2>
    </>
  );
};

export default Loader;
