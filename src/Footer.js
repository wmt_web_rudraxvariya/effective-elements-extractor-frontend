import React from "react";

const Footer = () => {
  return (
    <div
      style={{
        backgroundColor: "black",
        textAlign: "center",
        color: "white",
        padding: "20px",
      }}
    >
      &hearts;  
    </div>
  );
};

export default Footer;
